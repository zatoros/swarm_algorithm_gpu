#include<amp.h>
#include<cstdlib>

#include "AlghoritmAMP.h"

#include "CommonConst.h"
#include "MatrixHelper.h"
#include "DebugHelper.h"

using namespace concurrency;

AlghoritmAMP::AlghoritmAMP(AlghoritmSettings settings) : Alghoritm(settings)
{
}

void AlghoritmAMP::decodeSolution()
{
	LogMessage("Decodeing solution");
	PrintResult(globalBestValue,problemSize,"Before");
	int colBestIndex = 0;
	for(int i = 0; i < problemSize; i++)
	{
		colBestIndex = 0;
		for(int j = 1; j < problemSize; j++)
		{
			if(globalBestValue->Solution[j*problemSize + i] < globalBestValue->Solution[colBestIndex*problemSize + i])
			{
				globalBestValue->Solution[j*problemSize + i] = 0.0;
			}
			else
			{
				globalBestValue->Solution[colBestIndex*problemSize + i] = 0.0;
				colBestIndex = j;
			}
		}
		globalBestValue->Solution[colBestIndex*problemSize + i] = 1.0;
	}
	globalBestValue->Value = AlghoritmAMP::GoalFunction(facilities,locations,globalBestValue->Solution,problemSize);
	PrintResult(globalBestValue,problemSize,"After");
}

void AlghoritmAMP::MoveParticles()
{
	LogMessage("Moveing  particles");
	for(int i = 0; i < particleNumber; i++)
		particles[i].Move(intertia,cognitionLR,socialLR,r1,r2,globalBestValue->Solution,problemSize);
}

void AlghoritmAMP::UpdateParticlesValues()
{
	LogMessage("Checking particles best values");
	for(int i = 0; i < particleNumber; i++)
		particles[i].CheckBestValue(facilities,locations,problemSize);
}

void AlghoritmAMP::ParticleInit()
{
	LogMessage("Initializing particles");
	float * temp;
	int *rands = new int[problemSize];
	for(int i = 0; i < problemSize; i++)
		rands[i] = i;
	srand(seed);
	particles = new ParticleAMP[particleNumber];
	for(int i = 0; i < particleNumber; i++)
	{
		NewMatrix(&temp,problemSize);
		randomParticle(temp,rands);
		particles[i].InitParticle(temp,facilities,locations,problemSize);
	}
	delete[] rands;
}

float AlghoritmAMP::GoalFunction(float * facilities, float * locations, float * solution, int problemSize)
{
	LogMessage("Counting goal value AMP C++");

	float * result = new float[problemSize];

	array_view<float, 2> locationAmp(problemSize,problemSize,locations);
	array_view<float, 2> solutionAmp(problemSize,problemSize,solution);
	array_view<float, 2> facilitiestAmp(problemSize,problemSize,facilities);
	array_view<float, 1> resultAmp(problemSize,result);

	parallel_for_each(
		resultAmp.extent, 
		[=](index<1> idx) restrict(amp) {
			int i = idx[0];
			float sum = 0;
			for(int j = 0; j < problemSize; j++)
			{
				for(int r = 0; r < problemSize; r++)
				{
					for(int s = 0; s < problemSize; s++)
					{
						sum += facilitiestAmp(i,j)*locationAmp(r,s)*solutionAmp(i,r)*solutionAmp(j,s);
					}
				}
			}
			resultAmp(i) = sum;
	});
    resultAmp.synchronize();

	float retResult = 0;
	for(int i = 0; i < problemSize; i++)
		retResult += result[i];

	delete[] result;

	return retResult;
}

AlghoritmAMP::~AlghoritmAMP(void)
{
	LogMessage("Releasing AlghoritmAmp object");
}

void AlghoritmAMP::ParticleAMP::countVelocity(float intertia, float cognitionLR, float socialLR,
											  float r1, float r2, float * globalBest, int problemSize)
{
	LogMessage("Counting particle velocity AMP C++");
	
	array_view<float, 2> velocityAmp(problemSize,problemSize,velocity);
	array_view<float, 2> solutionAmp(problemSize,problemSize,solution);
	array_view<float, 2> globalBestAmp(problemSize,problemSize,globalBest);
	array_view<float, 2> localBestAmp(problemSize,problemSize,localBestValue->Solution);

	parallel_for_each(
        velocityAmp.extent, 
         [=](index<2> idx) restrict(amp) {
            int row = idx[0];
            int col = idx[1];
			velocityAmp(row,col) = velocityAmp(row,col)*intertia + cognitionLR*r1*(localBestAmp(row,col) - solutionAmp(row,col))
				+ socialLR*r2*(globalBestAmp(row,col) - solutionAmp(row,col));
        }
    );
    velocityAmp.synchronize();
}

void AlghoritmAMP::ParticleAMP::scaleSolution(int problemSize)
{
	LogMessage("Scaleing solution");
	PrintMatrix(solution,problemSize,"solution - before scaleing");
	float colSum;
	float * rowSums = new float[problemSize];
	for(int i = 0; i < problemSize; i++)
		rowSums[i] = 0.0;
	for(int i = 0; i < problemSize; i++)
	{
		colSum = 0;
		for(int j = 0; j < problemSize; j++)
		{
			colSum += solution[j*problemSize + i];
			rowSums[j] += solution[j*problemSize + i];
		}
		if(colSum < 1.0/float_PRECISION)
		{
			for(int j = 0; j < problemSize; j++)
			{
				solution[j*problemSize + i] = (rand() % float_PRECISION)/(float)float_PRECISION;
				rowSums[j] += solution[j*problemSize + i];
			}
		}
	}
	for(int i = 0; i < problemSize; i++)
	{
		for(int j = 0; j < problemSize;  j++)
		{
			solution[j*problemSize + i] /= rowSums[i];
		}
	}
	delete[] rowSums;
	PrintMatrix(solution,problemSize,"solution - after scaleing");
}

void AlghoritmAMP::ParticleAMP::CheckBestValue(float * facilities, float * locations, int problemSize)
{
	LogMessage("Compareing alghoritm best value");
	float currentGoalValue = AlghoritmAMP::GoalFunction(facilities,locations,solution,problemSize);
	if(currentGoalValue < localBestValue->Value)
	{
		CopyMatrix(solution,localBestValue->Solution,problemSize);
		localBestValue->Value = currentGoalValue;
	}
}

void AlghoritmAMP::ParticleAMP::Move(float intertia, float cognitionLR, float socialLR,
									 float r1, float r2, float * globalBest, int problemSize)
{
	LogMessage("Moveing particle AMP C++");
	countVelocity(intertia,cognitionLR,socialLR,r1,r2,globalBest,problemSize);

	array_view<float, 2> velocityAmp(problemSize,problemSize,velocity);
	array_view<float, 2> solutionAmp(problemSize,problemSize,solution);

	parallel_for_each(
        solutionAmp.extent, 
         [=](index<2> idx) restrict(amp) {
            int row = idx[0];
            int col = idx[1];
			solutionAmp[idx] += velocityAmp(row,col);
			if(solutionAmp[idx] < 0 || solutionAmp[idx] != solutionAmp[idx])
				solutionAmp[idx] = 0;
        }
    );
    solutionAmp.synchronize();

	scaleSolution(problemSize);
}

void AlghoritmAMP::ParticleAMP::InitParticle(float * initSolution, float * facilities, 
											 float * locations, int problemSize)
{
	LogMessage("Initializing ParticleAMP");
	Alghoritm::Particle::InitParticle(initSolution,facilities,locations,problemSize);
	localBestValue->Value = AlghoritmAMP::GoalFunction(facilities,locations,solution,problemSize);
}