#pragma once

//Indeksy parametrów lini polecen do konfiguracji aplikacji

static char const * PROBLEM_SIZE_LITERAL = "-n";

static char const * FACILITIES_LITERAL = "-F";

static char const * LOCATIONS_LITERAL = "-L";

static char const * INTERIA_LITERAL = "-i";

static char const * COGNITION_LITERAL = "-clr";

static char const * SOCIAL_LITERAL = "-slr";

static char const * R1_LITERAL = "-r1";

static char const * R2_LITERAL = "-r2";

static char const * SEED_LITERAL = "-s";

static char const * PARTICLE_NUMBER_LITERAL = "-pn";

static char const * EPSILON_LITERAL = "-eps";

static char const * MAX_ITERATION_LITERAL = "-mit";

static char const * ALGHORITM_TYPE_LITERAL = "-alt";

static char const * OUTPUT_FILE_LITERAL = "-outf";

static char const * PROGRAM_FILENAMES[] = { "GoalFunction.cl", "Move.cl", "CountVelocity.cl" };

static char const * KERNEL_NAMES_LITERALS[] = { "goalFunction", "move", "countVelocity" };