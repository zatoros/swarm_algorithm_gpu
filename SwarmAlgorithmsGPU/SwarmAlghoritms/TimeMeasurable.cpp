#include<fstream>
#include "TimeMeasurable.h"

using namespace std;

TimeMeasurable::TimeMeasurable(int iteration)
{
	itterationTime = new TimeStamp[iteration];
	updateingTime = new TimeStamp[iteration];
	moveingTime = new TimeStamp[iteration];
	results = new float[iteration];
	currIteration = 0;
}


TimeMeasurable::~TimeMeasurable(void)
{
	delete[] itterationTime;
	delete[] updateingTime;
	delete[] moveingTime;
	delete[] results;
}

void TimeMeasurable::logCollection(TimeStamp * stamps, std::ostream & out)
{
	for(int i = 0; i < currIteration; i++)
		out << stamps[i].Eclapsed() << " ";
	out << endl;
}

void TimeMeasurable::logCollection(float * stamps, std::ostream & out)
{
	for(int i = 0; i < currIteration; i++)
		out << stamps[i] << " ";
	out << endl;
}

void TimeMeasurable::SaveTimeReport(char * filePath)
{
	ofstream out(filePath);

	out << alghortimTime.Eclapsed() << endl;
	out << initializationTime.Eclapsed() << endl;

	logCollection(itterationTime,out);
	logCollection(updateingTime,out);
	logCollection(moveingTime,out);
	logCollection(results,out);
	out.close();
}
