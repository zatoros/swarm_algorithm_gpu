#include<cstdlib>

#include"MatrixHelper.h"
#include"Releasable.h"
#include"DebugHelper.h"

void ReleaseMatrix(float * matrix, int n)
{
	LogMessage("Release matrix");
	if(matrix != NULL)
	{
		delete[] matrix;
	}
}

void NewMatrix(float ** matrix,int n)
{
	LogMessage("New matrix");
	float * temp;
	temp = new float[n*n];
	*matrix = temp;
}

void CopyMatrix(float * from, float * to, int n)
{
	LogMessage("Copy matrix");
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			to[i*n + j] = from[i*n +j];
		}
	}
}

void DeepCopyMatrix(float * from, float ** to, int n)
{
	LogMessage("Deep copy matrix");
	NewMatrix(to,n);
	CopyMatrix(from,*to,n);
}

void ZeroMatrix(float * matrix,int n)
{
	LogMessage("Zero matrix");
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			matrix[i*n + j] = 0;
		}
	}
}

void ReleaseClass(Releasable * releasable,int n)
{
	LogMessage("Release class");
	if(releasable != NULL)
	{
		releasable->Release(n);
	}
	delete releasable;
}

void CopyResult(Alghoritm::BestSolution from, Alghoritm::BestSolution * to,int n)
{
	LogMessage("Copy result");
	CopyMatrix(from.Solution,to->Solution,n);
	to->Value = from.Value;
}