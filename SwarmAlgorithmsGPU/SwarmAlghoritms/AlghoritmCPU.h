#pragma once

#include "alghoritm.h"

class AlghoritmCPU : public Alghoritm
{
protected:
	void decodeSolution();
public:
	class ParticleCPU : public Alghoritm::Particle
	{
	protected:
		void countVelocity(float intertia, float cognitionLR, float socialLR,
			float r1, float r2, float * globalBest, int problemSize);
		void scaleSolution(int problemSize);
	public:
		void CheckBestValue(float * facilities, float * locations, int problemSize);
		void Move(float intertia, float cognitionLR, float socialLR,
			float r1, float r2, float * globalBest, int problemSize);
		void InitParticle(float * initSolution, float * facilities, float * locations, int problemSize);
	};
	void InitParticle(float * initSolution, float * facilities, float * locations, int problemSize);
	void MoveParticles();
	void UpdateParticlesValues();
	void ParticleInit();
	AlghoritmCPU(AlghoritmSettings settings);
	~AlghoritmCPU(void);
	static float GoalFunction(float * facilities, float * locations, float * solution, int problemSize);
};

