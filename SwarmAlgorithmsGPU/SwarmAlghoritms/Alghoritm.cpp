#include<stdlib.h>

#include "Alghoritm.h"
#include "MatrixHelper.h"
#include "CommonConst.h"
#include "DebugHelper.h"

Alghoritm::Alghoritm(AlghoritmSettings settings) : TimeMeasurable(settings.GetmMaxIteration())
{
	LogMessage("Initializing alghoritm object fields");
	problemSize = settings.GetProblemSize();
	PrintValue(problemSize,"problemSize - alghoritm");
	DeepCopyMatrix(settings.GetFacilities(),&facilities,problemSize);
	PrintMatrix(facilities,problemSize,"facilities - alghoritm");
	DeepCopyMatrix(settings.GetLocations(),&locations,problemSize);
	PrintMatrix(locations,problemSize,"locations - alghoritm");
	intertia = settings.GetIntertia();
	PrintValue(intertia,"intertia - alghoritm");
	cognitionLR = settings.GetCognitionLR();
	PrintValue(cognitionLR,"cognitionLR - alghoritm");
	socialLR = settings.GetSocialLR();
	PrintValue(socialLR,"socialLR - alghoritm");
	r1 = settings.GetR1();
	PrintValue(r1,"r1 - alghoritm");
	r2 = settings.GetR2();
	PrintValue(r2,"r2 - alghoritm");
	seed = settings.GetSeed();
	PrintValue(seed,"seed - alghoritm");
	particleNumber = settings.GetParticleNumber();
	PrintValue(particleNumber,"particleNumber - alghoritm");
	epsilon = settings.GetEpisilon();
	PrintValue(epsilon,"epsilon - alghoritm");
	maxIteration = settings.GetmMaxIteration();
	PrintValue(maxIteration,"maxIteration - alghoritm");
}

void Alghoritm::Start()
{
	LogMessage("Alghoritm starting");
	LogBeginAlghoritm();

	LogBegIninitialization();
	ParticleInit();
	ParametersInit();
	LogEndInitialization();

	do
	{
		LogBeginItteration();

		LogBeginUpdateing();
		UpdateParticlesValues();
		UpdateGlobalBest();
		LogEndUpdateing();

		LogBeginMoveing();
		MoveParticles();
		LogEndMoveing();

		++iteration;
		
		LogEndItteration();
		NextItteration();
		LogResult(globalBestValue->Value);
	}while(!IsStop());
	Finish();
	LogEndAlghoritm();
}

void Alghoritm::Finish()
{
	LogMessage("Alghoritm finished");
	decodeSolution();
	PrintResult(globalBestValue,problemSize,"Result");
}

bool Alghoritm::IsStop()
{
	LogMessage("Check if alghoritm stopped");
	PrintValue(iteration,"itteration");
	PrintValue(prevGlobalBestValue - globalBestValue->Value,"progress");
	bool isstop = false;
	if(iteration > maxIteration)
	{
		isstop = true;
	}
	else if( iteration >= 2  && 
			epsilon != -1 &&
		prevGlobalBestValue - globalBestValue->Value < epsilon)
	{
		isstop = true;
	}
	return isstop;
}

void Alghoritm::UpdateGlobalBest()
{
	prevGlobalBestValue = globalBestValue->Value;
	int bestIndex = -1;
	for(int i = 0; i < particleNumber; i++)
	{
		if(particles[i].GetBestSolution() < *globalBestValue)
		{
			bestIndex = i;
		}
	}
	if( bestIndex != -1 )
	{
		CopyResult(particles[bestIndex].GetBestSolution(),globalBestValue,problemSize);
	}
	PrintMatrix(globalBestValue->Solution,problemSize,"global best solution");
	PrintValue(globalBestValue->Value,"global best value");
}

void Alghoritm::randomParticle(float * particleSolution,int *rands)
{
	LogMessage("Initializing particles - random generation");
	int randomNumber;
	int temp;
	for(int i = 0; i < problemSize/2; i++)
	{
		randomNumber = rand() % problemSize;
		temp = rands[randomNumber];
		rands[randomNumber] = rands[i];
		rands[i] = temp;
	}
	for(int i = 0; i < problemSize; i++)
	{
		for(int j = 0; j < problemSize; j++)
		{
			particleSolution[i*problemSize + j] = (float)(j == rands[i] ? 1.0 : 0.0);
		}
	}
}

void Alghoritm::ParametersInit()
{
	LogMessage("Initializing alghoritm parameters values");
	globalBestValue = new BestSolution();
	NewMatrix(&globalBestValue->Solution,problemSize);
	iteration = 0;
	CopyResult(particles[0].GetBestSolution(),globalBestValue,problemSize);
}

Alghoritm::~Alghoritm(void)
{
	LogMessage("Releasing alghoritm object");
	ReleaseMatrix(facilities,problemSize);
	ReleaseMatrix(locations,problemSize);

	ReleaseClass(globalBestValue,problemSize);
	for(int i = 0; i < particleNumber; i++)
		particles[i].Release(problemSize);
}

Alghoritm::Particle::Particle()
{	
	LogMessage("Constructing particle");
	solution = NULL;
	localBestValue = NULL;
	velocity = NULL;
}

void Alghoritm::Particle::InitParticle(float * initSolution, float * facilities, 
											 float * locations, int problemSize)
{
	LogMessage("Initializing particle");
	solution = initSolution;
	PrintMatrix(solution,problemSize,"Initializing particle solution");
	localBestValue = new BestSolution();
	NewMatrix(&localBestValue->Solution,problemSize);
	NewMatrix(&velocity,problemSize);
	CopyMatrix(solution,localBestValue->Solution,problemSize);
	ZeroMatrix(velocity,problemSize);
}

void Alghoritm::Particle::Release(int n)
{
	LogMessage("Releasing particle");
	ReleaseMatrix(solution,n);
	ReleaseMatrix(velocity,n);
	ReleaseClass(localBestValue,n);
}

Alghoritm::BestSolution::BestSolution()
{
	LogMessage("Constructing solution");
	Solution = NULL;
	Value = 0;
}

void Alghoritm::BestSolution::Release(int n)
{
	LogMessage("Releasing solution");
	ReleaseMatrix(Solution,n);
}