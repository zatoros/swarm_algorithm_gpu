#include<iostream>

#include"DebugHelper.h"
#include"Alghoritm.h"
#include"GlobalSettings.h"

using namespace std;

void PrintMatrix(float * matrix, int n, char * name)
{
#ifdef PRINT_DEBUG_INFO
	cout << name << endl;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
			cout << matrix[i*n + j] << "\t";
		cout << endl;
	}
#endif
}

void PrintValue(double value, char * name)
{
#ifdef PRINT_DEBUG_INFO
	cout << name << " = " << value << endl;
#endif
}

void PrintResult(Alghoritm::BestSolution * solution,int n, char * name)
{
#ifdef PRINT_DEBUG_INFO
	cout << name << endl;
	PrintMatrix(solution->Solution,n,"Solution");
	PrintValue(solution->Value,"Value");
#endif
}

void LogMessage(char * message)
{
#ifdef PRINT_DEBUG_INFO
	cout << message << endl;
#endif
}