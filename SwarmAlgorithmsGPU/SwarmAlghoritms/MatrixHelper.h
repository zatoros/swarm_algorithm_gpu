#pragma once

#include"bolt\cl\device_vector.h"

#include"Releasable.h"
#include"Alghoritm.h"

//funkcja zwalnia pami�� z kwadratowej macierzy
void ReleaseMatrix(float * matrix, int n);

//kopiowanie kwadratowych macierzy - p�ytkie
void CopyMatrix(float * from, float * to, int n);

//tworzenie nowej kwadratowej macierzy
void NewMatrix(float ** matrix,int n);

//kopiowanie kwadratowych macierzy - p�ytkie
void DeepCopyMatrix(float * from, float ** to, int n);

//zerowanie macierzy
void ZeroMatrix(float * matrix,int n);

//funkcja zwalnia pami�c z klasy implementuj�cej interfejs
void ReleaseClass(Releasable * releasable,int n);

//Funkcja kopiuje rozwi�zania
void CopyResult(Alghoritm::BestSolution from, Alghoritm::BestSolution * to,int n);

//Funkcja sp�aszcza macierz :-)
//void FllaterMatrix(float ** from, float * to, int n);
//void FllaterMatrixToVector(float ** from, bolt::cl::device_vector<float> to, int n);

//Funkcja rozp�aszcza macierz
//void UnFllaterMatrix(float * from, float **to, int n);
//void UnFllaterMatrixToVector(bolt::cl::device_vector<float> from, float **to, int n);
