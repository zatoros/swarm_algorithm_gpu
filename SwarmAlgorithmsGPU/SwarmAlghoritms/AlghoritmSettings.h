#pragma once

#include "Releasable.h"

class AlghoritmSettings : public Releasable
{
private:
	//Rozmiar problemu (n)
	int problemSize;
	//Macierz wag F
	float *facilities;
	//Macierz odleg�o�ci L
	float *locations;
	//Metoda parsuje macierze 
	//zwraca index za now� warto�ci� i
	int parseDataMatrix(char * argv[], int startIndex, float ** matrix);
	//wsp�czynnik bezw�adno�ci
	float intertia;
	//waga �wiadomo�ci
	float cognitionLR;
	//waga my�lenia spo�ecznego
	float socialLR;
	//R1
	float r1;
	//R2
	float r2;
	//ziarno prawdopodobie�stwa
	int seed;
	//ilo�� cz�stek
	int particleNumber;
	//kryterium stopu - maksymalne iteracje
	int maxIteration;
	//kryterium stopu - poprawa rozwi�zania
	float epsilon;
	//typ algorytmu
	int alghoritm_type;
	//�cie�ka do pliku wynikowego
	char * outputPath;
	//Metoda inicjalizuje pola danych
	void initFields();
public:
	//publiczny konstruktor
	//argc - ilo�� argument�w lin polece�
	//argv - argumenty lini polece�
	AlghoritmSettings(int argc, char * argv[]);
	//Konstruktor kopiuj�cy
	AlghoritmSettings(AlghoritmSettings & copy);
	//Zwraca macierz wag F
	float * GetFacilities() const { return facilities; }
	//Zwraca macierz odleg�o�ci L
	float * GetLocations() const { return locations; }
	//Zwraca rozmiar problemu
	int GetProblemSize() const { return problemSize; }
	//Zwraca wsp�czynnik bezw�adno�ci
	float GetIntertia() const { return intertia; }
	//Zwraca wage �wiadomo�ci
	float GetCognitionLR() const { return cognitionLR; }
	//Zwraca wsp�czynnik my�lenia spo�ecznego
	float GetSocialLR() const { return socialLR; }
	//Zwraca R1
	float GetR1() const { return r1; }
	//Zwraca R2
	float GetR2() const { return r2; }
	//Zwraca ziarno prawdopodobie�stwa
	int GetSeed() const { return seed; }
	//Zwraca ilo�� cz�stek
	int GetParticleNumber() const { return particleNumber; }
	//Zwraca kryterium stopu - poprawe rowi�zania
	float GetEpisilon() const { return epsilon; }
	//Zwraca kryterium stopu - maksynalne iteracje
	int GetmMaxIteration() const { return maxIteration; }
	//zwraca typ algorytmu
	int GetAlghoritmType() const { return alghoritm_type; }
	//Zwraca �cie�ke do pliku wynikowego
	char * GetOutputPath() const { return outputPath; }
	//Zwalnia pami��
	virtual void Release(int n);
	//destruktor
	~AlghoritmSettings(void);
};

