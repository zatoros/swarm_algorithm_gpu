#include<cstdlib>
#include"bolt/cl/transform.h"

#include "AlghoritmBolt.h"

#include "CommonConst.h"
#include "MatrixHelper.h"
#include "DebugHelper.h"

BOLT_FUNCTOR(MoveFunctor,
struct MoveFunctor
{
    float operator() ( const float& xx, const float& yy )
    {
        float res = xx + yy;
		if(res < 0 || res != res)
			return 0;
		return res;
    };
};);


AlghoritmBolt::AlghoritmBolt(AlghoritmSettings settings) : Alghoritm(settings)
{
}

void AlghoritmBolt::decodeSolution()
{
	LogMessage("Decodeing solution");
	PrintResult(globalBestValue,problemSize,"Before");
	int colBestIndex = 0;
	for(int i = 0; i < problemSize; i++)
	{
		colBestIndex = 0;
		for(int j = 1; j < problemSize; j++)
		{
			if(globalBestValue->Solution[j][i] < globalBestValue->Solution[colBestIndex][i])
			{
				globalBestValue->Solution[j][i] = 0.0;
			}
			else
			{
				globalBestValue->Solution[colBestIndex][i] = 0.0;
				colBestIndex = j;
			}
		}
		globalBestValue->Solution[colBestIndex][i] = 1.0;
	}
	globalBestValue->Value = AlghoritmBolt::GoalFunction(facilities,locations,globalBestValue->Solution,problemSize);
	PrintResult(globalBestValue,problemSize,"After");
}

void AlghoritmBolt::MoveParticles()
{
	LogMessage("Moveing  particles");
	for(int i = 0; i < particleNumber; i++)
		particles[i].Move(intertia,cognitionLR,socialLR,r1,r2,globalBestValue->Solution,problemSize);
}

void AlghoritmBolt::UpdateParticlesValues()
{
	LogMessage("Checking particles best values");
	for(int i = 0; i < particleNumber; i++)
		particles[i].CheckBestValue(facilities,locations,problemSize);
}

void AlghoritmBolt::ParticleInit()
{
	LogMessage("Initializing particles");
	double ** temp;
	int *rands = new int[problemSize];
	for(int i = 0; i < problemSize; i++)
		rands[i] = i;
	srand(seed);
	particles = new ParticleBolt[particleNumber];
	for(int i = 0; i < particleNumber; i++)
	{
		NewMatrix(&temp,problemSize);
		randomParticle(temp,rands);
		particles[i].InitParticle(temp,facilities,locations,problemSize);
	}
	delete[] rands;
}

double AlghoritmBolt::GoalFunction(double ** facilities, double ** locations, double ** solution, int problemSize)
{
	LogMessage("Counting goal value AMP C++");

	bolt::cl::device_vector<float>  locationsFlat(problemSize*problemSize);
	bolt::cl::device_vector<float>  solutionFlat(problemSize*problemSize);
	bolt::cl::device_vector<float>  facilitiesFlat(problemSize*problemSize);
	bolt::cl::device_vector<float>  result(problemSize);

	FllaterMatrix(locations,locationsFlat,problemSize);
	FllaterMatrix(solution,solutionFlat,problemSize);
	FllaterMatrix(facilities,facilitiesFlat,problemSize);

	/*parallel_for_each(
		resultAmp.extent, 
		[=](index<1> idx) restrict(amp) {
			int i = idx[0];
			double sum = 0;
			for(int j = 0; j < problemSize; j++)
			{
				for(int r = 0; r < problemSize; r++)
				{
					for(int s = 0; s < problemSize; s++)
					{
						sum += facilitiestAmp(i,j)*locationAmp(r,s)*solutionAmp(i,r)*solutionAmp(j,s);
					}
				}
			}
			resultAmp(i) = sum;
	});*/

	double retResult = 0;
	for(int i = 0; i < problemSize; i++)
		retResult += result[i];

	return retResult;
}

AlghoritmBolt::~AlghoritmBolt(void)
{
	LogMessage("Releasing AlghoritmCPU object");
}

void AlghoritmBolt::ParticleBolt::countVelocity(double intertia, double cognitionLR, double socialLR,
											  double r1, double r2, double ** globalBest, int problemSize)
{
	LogMessage("Counting particle velocity AMP C++");
	
	bolt::cl::device_vector<float>  velocityFlat(problemSize*problemSize);
	bolt::cl::device_vector<float>  solutionFlat(problemSize*problemSize);
	bolt::cl::device_vector<float>  globalBestFlat(problemSize*problemSize);
	bolt::cl::device_vector<float>  localBestValueFlat(problemSize*problemSize);

	FllaterMatrix(velocity,velocityFlat,problemSize);
	FllaterMatrix(solution,solutionFlat,problemSize);
	FllaterMatrix(globalBest,globalBestFlat,problemSize);
	FllaterMatrix(localBestValue->Solution,localBestValueFlat,problemSize);

	/*parallel_for_each(
        velocityAmp.extent, 
         [=](index<2> idx) restrict(amp) {
            int row = idx[0];
            int col = idx[1];
			velocityAmp(row,col) = velocityAmp(row,col)*intertia + cognitionLR*r1*(localBestAmp(row,col) - solutionAmp(row,col))
				+ socialLR*r2*(globalBestAmp(row,col) - solutionAmp(row,col));
        }
    );*/

	UnFllaterMatrix(velocityFlat,velocity,problemSize);
}

void AlghoritmBolt::ParticleBolt::scaleSolution(int problemSize)
{
	LogMessage("Scaleing solution");
	PrintMatrix(solution,problemSize,"solution - before scaleing");
	double colSum;
	double * rowSums = new double[problemSize];
	for(int i = 0; i < problemSize; i++)
		rowSums[i] = 0.0;
	for(int i = 0; i < problemSize; i++)
	{
		colSum = 0;
		for(int j = 0; j < problemSize; j++)
		{
			colSum += solution[j][i];
			rowSums[j] += solution[j][i];
		}
		if(colSum < 1.0/DOUBLE_PRECISION)
		{
			for(int j = 0; j < problemSize; j++)
			{
				solution[j][i] = (rand() % DOUBLE_PRECISION)/(double)DOUBLE_PRECISION;
				rowSums[j] += solution[j][i];
			}
		}
	}
	for(int i = 0; i < problemSize; i++)
	{
		for(int j = 0; j < problemSize;  j++)
		{
			solution[j][i] /= rowSums[i];
		}
	}
	delete[] rowSums;
	PrintMatrix(solution,problemSize,"solution - after scaleing");
}

void AlghoritmBolt::ParticleBolt::CheckBestValue(double ** facilities, double ** locations, int problemSize)
{
	LogMessage("Compareing alghoritm best value");
	double currentGoalValue = AlghoritmBolt::GoalFunction(facilities,locations,solution,problemSize);
	if(currentGoalValue < localBestValue->Value)
	{
		CopyMatrix(solution,localBestValue->Solution,problemSize);
		localBestValue->Value = currentGoalValue;
	}
}

void AlghoritmBolt::ParticleBolt::Move(double intertia, double cognitionLR, double socialLR,
									 double r1, double r2, double ** globalBest, int problemSize)
{
	LogMessage("Moveing particle AMP C++");
	countVelocity(intertia,cognitionLR,socialLR,r1,r2,globalBest,problemSize);

	bolt::cl::device_vector<float> velocityFlat(problemSize*problemSize);
	bolt::cl::device_vector<float> solutionFlat(problemSize*problemSize);

	FllaterMatrix(velocity,velocityFlat,problemSize);
	FllaterMatrix(solution,solutionFlat,problemSize);

	MoveFunctor func;
	bolt::cl::transform(solutionFlat.begin(),solutionFlat.end(),velocityFlat.begin(),solutionFlat.begin(),func);

	UnFllaterMatrix(solutionFlat,solution,problemSize);

	scaleSolution(problemSize);
}

void AlghoritmBolt::ParticleBolt::InitParticle(double ** initSolution, double ** facilities, 
											 double ** locations, int problemSize)
{
	LogMessage("Initializing ParticleAMP");
	Alghoritm::Particle::InitParticle(initSolution,facilities,locations,problemSize);
	localBestValue->Value = AlghoritmBolt::GoalFunction(facilities,locations,solution,problemSize);
}