#pragma once

#include<CL\cl.h>
#include "alghoritm.h"

//Singleton pomocniczy do 
class OpenClHelper
{
public:
	//enum do kierowania po programach
	enum PROGRAM_TYPE { GOAL_FUNCTION = 0, MOVE_FUNCTION = 1, COUNT_VELOCITY_FUNCTION = 2 }; 
private:
        OpenClHelper() {}
        OpenClHelper(const OpenClHelper &);
        OpenClHelper& operator=(const OpenClHelper&);
        ~OpenClHelper() {}
		//Konwertuje plik z kernelem na stringa
		void convertToString(const char *filename, char ** s);
		//Buduje program i zapisuje do zmiennej
		void buildProgram(enum PROGRAM_TYPE type, const char * filename);
		//Kontekst
		cl_context context;
		//urz�dzenia openCL
		cl_device_id * devices;
		//kolejka
		cl_command_queue commandQueue;
		//Zmienne na 3 programy do oblicze�
		cl_program program[3];
public:
	//inicjalizuje zmienne OpenCL
	void initOpenCLThings();
	//Zwalnianie zasob�w OpenCL
	void Release();
	//gettery
	cl_context GetContext() { return context; }
	cl_device_id * GetDevices() { return devices; }
	cl_command_queue GetCommandQueue() { return commandQueue; }
	cl_program GetProgram(enum PROGRAM_TYPE type) { return program[type]; }
	//zwraca instancje klasy
	static OpenClHelper * Instance();
};

class AlghoritmCL : public Alghoritm
{
protected:
	void decodeSolution();
public:
	class ParticleCL : public Alghoritm::Particle
	{
	protected:
		void countVelocity(float intertia, float cognitionLR, float socialLR,
			float r1, float r2, float * globalBest, int problemSize);
		void scaleSolution(int problemSize);
	public:
		void CheckBestValue(float * facilities, float * locations, int problemSize);
		void Move(float intertia, float cognitionLR, float socialLR,
			float r1, float r2, float * globalBest, int problemSize);
		void InitParticle(float * initSolution, float * facilities, float * locations, int problemSize);
	};
	void InitParticle(float * initSolution, float ** facilities, float * locations, int problemSize);
	void MoveParticles();
	void UpdateParticlesValues();
	void ParticleInit();
	AlghoritmCL(AlghoritmSettings settings);
	~AlghoritmCL(void);
	static float GoalFunction(float * facilities, float * locations, float * solution, int problemSize);
};

