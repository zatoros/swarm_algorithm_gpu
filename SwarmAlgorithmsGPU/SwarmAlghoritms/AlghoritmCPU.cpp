#include<cstdlib>

#include "AlghoritmCPU.h"

#include "CommonConst.h"
#include "MatrixHelper.h"
#include "DebugHelper.h"

AlghoritmCPU::AlghoritmCPU(AlghoritmSettings settings) : Alghoritm(settings)
{
}

void AlghoritmCPU::decodeSolution()
{
	LogMessage("Decodeing solution");
	PrintResult(globalBestValue,problemSize,"Before");
	int colBestIndex = 0;
	for(int i = 0; i < problemSize; i++)
	{
		colBestIndex = 0;
		for(int j = 1; j < problemSize; j++)
		{
			if(globalBestValue->Solution[j*problemSize + i] < globalBestValue->Solution[colBestIndex*problemSize + i])
			{
				globalBestValue->Solution[j*problemSize + i] = 0.0;
			}
			else
			{
				globalBestValue->Solution[colBestIndex*problemSize + i] = 0.0;
				colBestIndex = j;
			}
		}
		globalBestValue->Solution[colBestIndex*problemSize + i] = 1.0;
	}
	globalBestValue->Value = AlghoritmCPU::GoalFunction(facilities,locations,globalBestValue->Solution,problemSize);
	PrintResult(globalBestValue,problemSize,"After");
}

void AlghoritmCPU::MoveParticles()
{
	LogMessage("Moveing  particles");
	for(int i = 0; i < particleNumber; i++)
		particles[i].Move(intertia,cognitionLR,socialLR,r1,r2,globalBestValue->Solution,problemSize);
}

void AlghoritmCPU::UpdateParticlesValues()
{
	LogMessage("Checking particles best values");
	for(int i = 0; i < particleNumber; i++)
		particles[i].CheckBestValue(facilities,locations,problemSize);
}

void AlghoritmCPU::ParticleInit()
{
	LogMessage("Initializing particles");
	float * temp;
	int *rands = new int[problemSize];
	for(int i = 0; i < problemSize; i++)
		rands[i] = i;
	srand(seed);
	particles = new ParticleCPU[particleNumber];
	for(int i = 0; i < particleNumber; i++)
	{
		NewMatrix(&temp,problemSize);
		randomParticle(temp,rands);
		particles[i].InitParticle(temp,facilities,locations,problemSize);
	}
	delete[] rands;
}

float AlghoritmCPU::GoalFunction(float * facilities, float * locations, float * solution, int problemSize)
{
	LogMessage("Counting goal value");
	float result = 0;
	for(int i = 0; i < problemSize; i++)
	{
		for(int j = 0; j < problemSize; j++)
		{
			for(int r = 0; r < problemSize; r++)
			{
				for(int s = 0; s < problemSize; s++)
				{
					result += facilities[i*problemSize + j]*locations[r*problemSize + s]*solution[i*problemSize + r]*solution[j*problemSize + s];
				}
			}
		}
	}
	return result;
}

AlghoritmCPU::~AlghoritmCPU(void)
{
	LogMessage("Releasing AlghoritmCPU object");
}

void AlghoritmCPU::ParticleCPU::countVelocity(float intertia, float cognitionLR, float socialLR,
											  float r1, float r2, float * globalBest, int problemSize)
{
	LogMessage("Counting particle velocity");
	for(int i = 0; i < problemSize; i++)
	{
		for(int j = 0; j < problemSize; j++)
		{
			velocity[i*problemSize + j] = velocity[i*problemSize + j]*intertia + cognitionLR*r1*(localBestValue->Solution[i*problemSize + j] - solution[i*problemSize + j])
				+ socialLR*r2*(globalBest[i*problemSize + j] - solution[i*problemSize + j]);
		}
	}
}

void AlghoritmCPU::ParticleCPU::scaleSolution(int problemSize)
{
	LogMessage("Scaleing solution");
	PrintMatrix(solution,problemSize,"solution - before scaleing");
	float colSum;
	float * rowSums = new float[problemSize];
	for(int i = 0; i < problemSize; i++)
		rowSums[i] = 0.0;
	for(int i = 0; i < problemSize; i++)
	{
		colSum = 0;
		for(int j = 0; j < problemSize; j++)
		{
			colSum += solution[j*problemSize + i];
			rowSums[j] += solution[j*problemSize + i];
		}
		if(colSum < 1.0/float_PRECISION)
		{
			for(int j = 0; j < problemSize; j++)
			{
				solution[j*problemSize + i] = (rand() % float_PRECISION)/(float)float_PRECISION;
				rowSums[j] += solution[j*problemSize + i];
			}
		}
	}
	for(int i = 0; i < problemSize; i++)
	{
		for(int j = 0; j < problemSize;  j++)
		{
			solution[j*problemSize + i] /= rowSums[i];
		}
	}
	delete[] rowSums;

	PrintMatrix(solution,problemSize,"solution - after scaleing");
}

void AlghoritmCPU::ParticleCPU::CheckBestValue(float * facilities, float * locations, int problemSize)
{
	LogMessage("Compareing alghoritm best value");
	float currentGoalValue = AlghoritmCPU::GoalFunction(facilities,locations,solution,problemSize);
	if(currentGoalValue < localBestValue->Value)
	{
		CopyMatrix(solution,localBestValue->Solution,problemSize);
		localBestValue->Value = currentGoalValue;
	}
}

void AlghoritmCPU::ParticleCPU::Move(float intertia, float cognitionLR, float socialLR,
									 float r1, float r2, float * globalBest, int problemSize)
{
	LogMessage("Moveing particle");
	countVelocity(intertia,cognitionLR,socialLR,r1,r2,globalBest,problemSize);
	for(int i = 0; i < problemSize; i++)
	{
		for(int j = 0; j < problemSize; j++)
		{
			solution[i*problemSize + j] += velocity[i*problemSize + j];
			if(solution[i*problemSize + j] < 0 || solution[i*problemSize + j] != solution[i*problemSize + j])
				solution[i*problemSize + j] = 0;
		}
	}
	scaleSolution(problemSize);
}

void AlghoritmCPU::ParticleCPU::InitParticle(float * initSolution, float * facilities, 
											 float * locations, int problemSize)
{
	LogMessage("Initializing ParticleCPU");
	Alghoritm::Particle::InitParticle(initSolution,facilities,locations,problemSize);
	localBestValue->Value = AlghoritmCPU::GoalFunction(facilities,locations,solution,problemSize);
}