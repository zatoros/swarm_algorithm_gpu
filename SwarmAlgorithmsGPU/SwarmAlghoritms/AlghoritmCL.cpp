#include<CL\cl.h>
#include<cstdlib>
#include<fstream>

#include "AlghoritmCL.h"

#include "Literals.h"
#include "CommonConst.h"
#include "MatrixHelper.h"
#include "DebugHelper.h"

void OpenClHelper::convertToString(const char *filename, char ** s)
{
	size_t size;
	char*  str;
	std::fstream f(filename, (std::fstream::in | std::fstream::binary));

	if(f.is_open())
	{
		size_t fileSize;
		f.seekg(0, std::fstream::end);
		size = fileSize = (size_t)f.tellg();
		f.seekg(0, std::fstream::beg);
		str = new char[size+1];

		f.read(str, fileSize);
		f.close();
		str[size] = '\0';
		*s = str;
	}
}

void OpenClHelper::initOpenCLThings()
{
	LogMessage("Initializing OpenCL variables");
	/*Step1: Getting platforms and choose an available one.*/
	cl_uint numPlatforms;	//the NO. of platforms
	cl_platform_id platform = NULL;	//the chosen platform
	cl_int	status = clGetPlatformIDs(0, NULL, &numPlatforms);

	/*For clarity, choose the first available platform. */
	if(numPlatforms > 0)
	{
		cl_platform_id* platforms = (cl_platform_id* )malloc(numPlatforms* sizeof(cl_platform_id));
		status = clGetPlatformIDs(numPlatforms, platforms, NULL);
		platform = platforms[0];
		free(platforms);
	}

	/*Step 2:Query the platform and choose the first GPU device if has one.Otherwise use the CPU as device.*/
	cl_uint	numDevices = 0;
	status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &numDevices);	
	if (numDevices == 0)	//no GPU available.
	{
		status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 0, NULL, &numDevices);	
		devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
		status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, numDevices, devices, NULL);
	}
	else
	{
		devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
		status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, numDevices, devices, NULL);
	}

	/*Step 3: Create context.*/
	context = clCreateContext(NULL,1, devices,NULL,NULL,NULL);
	
	/*Step 4: Creating command queue associate with the context.*/
	commandQueue = clCreateCommandQueue(context, devices[0], 0, NULL);

	/*Step 5: Create program object */
	char *filename[] = { "GoalFunction.cl", "Move.cl", "CountVelocity.cl" };
	buildProgram(PROGRAM_TYPE::GOAL_FUNCTION, PROGRAM_FILENAMES[PROGRAM_TYPE::GOAL_FUNCTION]);
	buildProgram(PROGRAM_TYPE::COUNT_VELOCITY_FUNCTION, PROGRAM_FILENAMES[PROGRAM_TYPE::COUNT_VELOCITY_FUNCTION]);
	buildProgram(PROGRAM_TYPE::MOVE_FUNCTION, PROGRAM_FILENAMES[PROGRAM_TYPE::MOVE_FUNCTION]);
}

void OpenClHelper::buildProgram(enum PROGRAM_TYPE type, const char * filename)
{
	char * sourceString;
	convertToString(filename, &sourceString);
	size_t sourceSize[] = {strlen(sourceString)};
	program[type] = clCreateProgramWithSource(context, 1, (const char **)&sourceString, sourceSize, NULL);

	cl_int	status = clBuildProgram(program[type], 1,devices,NULL,NULL,NULL);

	PrintValue(type, " - program compilation");
	//Taka tempa diagnostyka
	/*if(status != CL_SUCCESS)
	{
		char buildLog[16384];
		clGetProgramBuildInfo(program[type], devices[0], CL_PROGRAM_BUILD_LOG,
		sizeof(buildLog), buildLog, NULL);
		LogMessage("Error in kernel: ");
		LogMessage(buildLog);
	}*/

	delete[] sourceString;
}

void OpenClHelper::Release()
{
	for(int i = 0; i < 3; i++)
		clReleaseProgram(program[i]);
	clReleaseCommandQueue(commandQueue);	
	clReleaseContext(context);	
	if (devices != NULL)
	{
		free(devices);
		devices = NULL;
	}
}

OpenClHelper * OpenClHelper::Instance()
{
	static OpenClHelper instance;
	return &instance;
}

AlghoritmCL::AlghoritmCL(AlghoritmSettings settings) : Alghoritm(settings)
{
}

void AlghoritmCL::decodeSolution()
{
	LogMessage("Decodeing solution");
	PrintResult(globalBestValue,problemSize,"Before");
	int colBestIndex = 0;
	for(int i = 0; i < problemSize; i++)
	{
		colBestIndex = 0;
		for(int j = 1; j < problemSize; j++)
		{
			if(globalBestValue->Solution[j*problemSize + i] < globalBestValue->Solution[colBestIndex*problemSize + i])
			{
				globalBestValue->Solution[j*problemSize + i] = 0.0;
			}
			else
			{
				globalBestValue->Solution[colBestIndex*problemSize + i] = 0.0;
				colBestIndex = j;
			}
		}
		globalBestValue->Solution[colBestIndex*problemSize + i] = 1.0;
	}
	globalBestValue->Value = AlghoritmCL::GoalFunction(facilities,locations,globalBestValue->Solution,problemSize);
	PrintResult(globalBestValue,problemSize,"After");
}

void AlghoritmCL::MoveParticles()
{
	LogMessage("Moveing  particles");
	for(int i = 0; i < particleNumber; i++)
		particles[i].Move(intertia,cognitionLR,socialLR,r1,r2,globalBestValue->Solution,problemSize);
}

void AlghoritmCL::UpdateParticlesValues()
{
	LogMessage("Checking particles best values");
	for(int i = 0; i < particleNumber; i++)
		particles[i].CheckBestValue(facilities,locations,problemSize);
}

void AlghoritmCL::ParticleInit()
{
	LogMessage("Initializing particles");
	OpenClHelper::Instance()->initOpenCLThings();
	float * temp;
	int *rands = new int[problemSize];
	for(int i = 0; i < problemSize; i++)
		rands[i] = i;
	srand(seed);
	particles = new ParticleCL[particleNumber];
	for(int i = 0; i < particleNumber; i++)
	{
		NewMatrix(&temp,problemSize);
		randomParticle(temp,rands);
		particles[i].InitParticle(temp,facilities,locations,problemSize);
	}
	delete[] rands;
}

float AlghoritmCL::GoalFunction(float * facilities, float * locations, float * solution, int problemSize)
{
	LogMessage("Counting goal value OpenCL");
	cl_int status;

	float * result = new float[problemSize];

	OpenClHelper * clHelper = OpenClHelper::Instance();
	cl_kernel kernel = clCreateKernel(clHelper->GetProgram(OpenClHelper::PROGRAM_TYPE::GOAL_FUNCTION),KERNEL_NAMES_LITERALS[OpenClHelper::PROGRAM_TYPE::GOAL_FUNCTION], &status);

	cl_mem resultBuffer = clCreateBuffer(clHelper->GetContext(), CL_MEM_READ_WRITE| CL_MEM_COPY_HOST_PTR, problemSize * sizeof(float),(void *) result, NULL);
	cl_mem facilitiesBuffer = clCreateBuffer(clHelper->GetContext(), CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, problemSize*problemSize * sizeof(float),(void *) facilities, NULL);
	cl_mem locationsBuffer = clCreateBuffer(clHelper->GetContext(), CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, problemSize*problemSize * sizeof(float),(void *) locations, NULL);
	cl_mem solutionBuffer = clCreateBuffer(clHelper->GetContext(), CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, problemSize*problemSize * sizeof(float),(void *) solution, NULL);

	status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&resultBuffer);
	status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&facilitiesBuffer);
	status = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&locationsBuffer);
	status = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&solutionBuffer);
	status = clSetKernelArg(kernel, 4, sizeof(int), (void *)&problemSize);

	size_t gWorkSize[1] = { problemSize };

	status = clEnqueueNDRangeKernel(clHelper->GetCommandQueue(), kernel, 1, NULL, gWorkSize, NULL, 0, NULL, NULL);
	status = clFinish(clHelper->GetCommandQueue());
	status = clEnqueueReadBuffer(clHelper->GetCommandQueue(), resultBuffer, CL_TRUE, 0, problemSize * sizeof(float), result, 0, NULL, NULL);

	float retResult = 0;
	for(int i = 0; i < problemSize; i++)
		retResult += result[i];

	delete[] result;
	clReleaseKernel(kernel);
	clReleaseMemObject(resultBuffer);
	clReleaseMemObject(facilitiesBuffer);
	clReleaseMemObject(locationsBuffer);
	clReleaseMemObject(solutionBuffer);
	return retResult;
}

AlghoritmCL::~AlghoritmCL(void)
{
	LogMessage("Releasing AlghoritmCL object");
	OpenClHelper::Instance()->Release();
}

void AlghoritmCL::ParticleCL::countVelocity(float intertia, float cognitionLR, float socialLR,
											  float r1, float r2, float * globalBest, int problemSize)
{
	LogMessage("Counting particle velocity OpenCL");
	
	cl_int status;

	OpenClHelper * clHelper = OpenClHelper::Instance();
	cl_kernel kernel = clCreateKernel(clHelper->GetProgram(OpenClHelper::PROGRAM_TYPE::COUNT_VELOCITY_FUNCTION),KERNEL_NAMES_LITERALS[OpenClHelper::PROGRAM_TYPE::COUNT_VELOCITY_FUNCTION], &status);

	cl_mem velocityBuffer = clCreateBuffer(clHelper->GetContext(), CL_MEM_READ_WRITE| CL_MEM_COPY_HOST_PTR, problemSize*problemSize * sizeof(float),(void *) velocity, NULL);
	cl_mem solutionBuffer = clCreateBuffer(clHelper->GetContext(), CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, problemSize*problemSize * sizeof(float),(void *) solution, NULL);
	cl_mem globbalBestBuffer = clCreateBuffer(clHelper->GetContext(), CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, problemSize*problemSize * sizeof(float),(void *) globalBest, NULL);
	cl_mem localBestBuffer = clCreateBuffer(clHelper->GetContext(), CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, problemSize*problemSize * sizeof(float),(void *) localBestValue->Solution, NULL);

	status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&velocityBuffer);
	status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&solutionBuffer);
	status = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&globbalBestBuffer);
	status = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&localBestBuffer);
	status = clSetKernelArg(kernel, 4, sizeof(float), (void *)&intertia);
	status = clSetKernelArg(kernel, 5, sizeof(float), (void *)&cognitionLR);
	status = clSetKernelArg(kernel, 6, sizeof(float), (void *)&socialLR);
	status = clSetKernelArg(kernel, 7, sizeof(float), (void *)&r1);
	status = clSetKernelArg(kernel, 8, sizeof(float), (void *)&r2);
	status = clSetKernelArg(kernel, 9, sizeof(int), (void *)&problemSize);

	size_t gWorkSize[2] = { problemSize, problemSize };

	clEnqueueNDRangeKernel(clHelper->GetCommandQueue(), kernel, 2, NULL, gWorkSize, NULL, 0, NULL, NULL);
	clFinish(clHelper->GetCommandQueue());
	clEnqueueReadBuffer(clHelper->GetCommandQueue(), velocityBuffer, CL_TRUE, 0, problemSize*problemSize * sizeof(float), velocity, 0, NULL, NULL);
	clReleaseKernel(kernel);
	clReleaseMemObject(velocityBuffer);
	clReleaseMemObject(solutionBuffer);
	clReleaseMemObject(globbalBestBuffer);
	clReleaseMemObject(localBestBuffer);
}

void AlghoritmCL::ParticleCL::scaleSolution(int problemSize)
{
	LogMessage("Scaleing solution");
	PrintMatrix(solution,problemSize,"solution - before scaleing");
	float colSum;
	float * rowSums = new float[problemSize];
	for(int i = 0; i < problemSize; i++)
		rowSums[i] = 0.0;
	for(int i = 0; i < problemSize; i++)
	{
		colSum = 0;
		for(int j = 0; j < problemSize; j++)
		{
			colSum += solution[j*problemSize + i];
			rowSums[j] += solution[j*problemSize + i];
		}
		if(colSum < 1.0/float_PRECISION)
		{
			for(int j = 0; j < problemSize; j++)
			{
				solution[j*problemSize + i] = (rand() % float_PRECISION)/(float)float_PRECISION;
				rowSums[j] += solution[j*problemSize + i];
			}
		}
	}
	for(int i = 0; i < problemSize; i++)
	{
		for(int j = 0; j < problemSize;  j++)
		{
			solution[j*problemSize + i] /= rowSums[i];
		}
	}
	delete[] rowSums;
	PrintMatrix(solution,problemSize,"solution - after scaleing");
}

void AlghoritmCL::ParticleCL::CheckBestValue(float * facilities, float * locations, int problemSize)
{
	LogMessage("Compareing alghoritm best value");
	float currentGoalValue = AlghoritmCL::GoalFunction(facilities,locations,solution,problemSize);
	if(currentGoalValue < localBestValue->Value)
	{
		CopyMatrix(solution,localBestValue->Solution,problemSize);
		localBestValue->Value = currentGoalValue;
	}
}

void AlghoritmCL::ParticleCL::Move(float intertia, float cognitionLR, float socialLR,
									 float r1, float r2, float * globalBest, int problemSize)
{
	LogMessage("Moveing particle OpenCL");
	countVelocity(intertia,cognitionLR,socialLR,r1,r2,globalBest,problemSize);

	cl_int status;

	OpenClHelper * clHelper = OpenClHelper::Instance();
	cl_kernel kernel = clCreateKernel(clHelper->GetProgram(OpenClHelper::PROGRAM_TYPE::MOVE_FUNCTION),KERNEL_NAMES_LITERALS[OpenClHelper::PROGRAM_TYPE::MOVE_FUNCTION], &status);

	cl_mem solutionBuffer = clCreateBuffer(clHelper->GetContext(), CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, problemSize*problemSize * sizeof(float),(void *) solution, NULL);
	cl_mem velocityBuffer = clCreateBuffer(clHelper->GetContext(), CL_MEM_READ_ONLY| CL_MEM_COPY_HOST_PTR, problemSize*problemSize * sizeof(float),(void *) velocity, NULL);

	status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&solutionBuffer);
	status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&velocityBuffer);
	status = clSetKernelArg(kernel, 2, sizeof(int), (void *)&problemSize);

	size_t gWorkSize[2] = { problemSize, problemSize };

	clEnqueueNDRangeKernel(clHelper->GetCommandQueue(), kernel, 2, NULL, gWorkSize, NULL, 0, NULL, NULL);
	clFinish(clHelper->GetCommandQueue());
	clEnqueueReadBuffer(clHelper->GetCommandQueue(), solutionBuffer, CL_TRUE, 0, problemSize*problemSize * sizeof(float), solution, 0, NULL, NULL);

	scaleSolution(problemSize);

	clReleaseKernel(kernel);
	clReleaseMemObject(solutionBuffer);
	clReleaseMemObject(velocityBuffer);
}

void AlghoritmCL::ParticleCL::InitParticle(float * initSolution, float * facilities, 
											 float * locations, int problemSize)
{
	LogMessage("Initializing ParticleCL");
	Alghoritm::Particle::InitParticle(initSolution,facilities,locations,problemSize);
	localBestValue->Value = AlghoritmCL::GoalFunction(facilities,locations,solution,problemSize);
}