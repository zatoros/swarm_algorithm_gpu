#pragma once
#include<ctime>
#include<iostream>

class TimeMeasurable
{
public:
	struct TimeStamp
	{
		clock_t Begin;
		clock_t End;
		void LogBegin() { Begin = clock(); }
		void LogEnd() { End = clock(); }
		float Eclapsed() { return float(float(End - Begin)/CLOCKS_PER_SEC*1000.); }
	};
private:
	TimeStamp initializationTime;
	TimeStamp alghortimTime;
	TimeStamp * itterationTime;
	TimeStamp * updateingTime;
	TimeStamp * moveingTime;
	float * results;
	int currIteration;
	void logBeginTime(TimeStamp * stamps) { stamps[currIteration].LogBegin(); }
	void logEndTime(TimeStamp * stamps) { stamps[currIteration].LogEnd(); }
	void logCollection(TimeStamp * stamps, std::ostream & out);
	void logCollection(float * stamps, std::ostream & out);
public:
	TimeMeasurable(int iteration);
	~TimeMeasurable(void);
	void NextItteration() { currIteration++; }
	void LogBeginAlghoritm() { alghortimTime.LogBegin(); }
	void LogEndAlghoritm() { alghortimTime.LogEnd(); }
	void LogBeginItteration() { logBeginTime(itterationTime); }
	void LogEndItteration() { logEndTime(itterationTime); }
	void LogBeginUpdateing() { logBeginTime(updateingTime); }
	void LogEndUpdateing() { logEndTime(updateingTime); }
	void LogBeginMoveing() { logBeginTime(moveingTime); }
	void LogEndMoveing() { logEndTime(moveingTime); }
	void LogBegIninitialization() { initializationTime.LogBegin(); }
	void LogEndInitialization() { initializationTime.LogEnd(); }
	void LogResult(float result) { results[currIteration] = result; }
	void SaveTimeReport(char * filePath);
};

