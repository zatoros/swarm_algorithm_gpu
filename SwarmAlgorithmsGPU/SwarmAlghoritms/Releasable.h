#pragma once

//Klasa dla obiekt�w z przydzielanymi dynamicznie macierzami
class Releasable
{
public:
	virtual void Release(int n) = 0;
};