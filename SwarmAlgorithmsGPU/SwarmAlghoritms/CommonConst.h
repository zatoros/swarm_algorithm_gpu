#pragma once

//Dokładność losowanych liczb zmiennoprzecinkowych
const int float_PRECISION = 1000;

//Typy algorytmow
const int ALGHORITM_CPU = 0;
const int ALGHORITM_AMP = 1;
const int ALGHORITM_CL = 2;
const int ALGHORITM_BOLT = 3;