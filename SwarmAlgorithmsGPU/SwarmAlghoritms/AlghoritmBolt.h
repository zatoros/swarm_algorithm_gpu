#pragma once

#include "alghoritm.h"

class AlghoritmBolt: public Alghoritm
{
protected:
	void decodeSolution();
public:
	class ParticleBolt : public Alghoritm::Particle
	{
	protected:
		void countVelocity(double intertia, double cognitionLR, double socialLR,
			double r1, double r2, double ** globalBest, int problemSize);
		void scaleSolution(int problemSize);
	public:
		void CheckBestValue(double ** facilities, double ** locations, int problemSize);
		void Move(double intertia, double cognitionLR, double socialLR,
			double r1, double r2, double ** globalBest, int problemSize);
		void InitParticle(double ** initSolution, double ** facilities, double ** locations, int problemSize);
	};
	void InitParticle(double ** initSolution, double ** facilities, double ** locations, int problemSize);
	void MoveParticles();
	void UpdateParticlesValues();
	void ParticleInit();
	AlghoritmBolt(AlghoritmSettings settings);
	~AlghoritmBolt(void);
	static double GoalFunction(double ** facilities, double ** locations, double ** solution, int problemSize);
};
