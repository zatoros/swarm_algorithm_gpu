
__kernel void countVelocity(__global float* velocity, __global float* solution,__global float* globalBest,
							__global float* localBestValue,
							const float intertia, const  float cognitionLR, const  float socialLR,
							const float r1, const float r2,
							const int problemSize)
{
	int row = get_global_id(0);
	int col = get_global_id(1);
	velocity[row*problemSize + col] = velocity[row*problemSize + col]*intertia 
		+ cognitionLR*r1*(localBestValue[row*problemSize + col] - solution[row*problemSize + col])
		+ socialLR*r2*(globalBest[row*problemSize + col] - solution[row*problemSize + col]);
}