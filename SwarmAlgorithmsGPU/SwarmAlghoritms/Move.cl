
__kernel void move(__global float* solution, __global float* velocity,
							const int problemSize)
{
	int row = get_global_id(0);
	int col = get_global_id(1);
	solution[row*problemSize + col] += velocity[row*problemSize + col];
	if(solution[row*problemSize + col] < 0 || solution[row*problemSize + col] != solution[row*problemSize + col])
		solution[row*problemSize + col] = 0;
}