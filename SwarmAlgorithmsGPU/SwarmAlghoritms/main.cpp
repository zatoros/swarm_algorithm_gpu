#include<iostream>
#include<CL\cl.h>

#include"AlghoritmSettings.h"
#include"AlghoritmCPU.h"
#include"AlghoritmAMP.h"
#include"AlghoritmCL.h"
#include"DebugHelper.h"
#include"CommonConst.h"

using namespace std;

int main(int argc, char * argv[])
{
	LogMessage("Loading settings");
	AlghoritmSettings settings(argc,argv);
	LogMessage("Prepareing alghoritm object");
	Alghoritm * Alghoritm;
	switch (settings.GetAlghoritmType())
	{
	case ALGHORITM_CPU:
		Alghoritm = new AlghoritmCPU(settings);
		break;
	case ALGHORITM_AMP:
		Alghoritm = new AlghoritmAMP(settings);
		break;
	case ALGHORITM_CL:
		Alghoritm = new AlghoritmCL(settings);
		break;
	}
	LogMessage("Starting alghoritm");
	Alghoritm->Start();
	LogMessage("Alghoritm fininished");
	LogMessage("Saveing results");
	Alghoritm->SaveTimeReport(settings.GetOutputPath());
	settings.Release(0);
	return 0;
}