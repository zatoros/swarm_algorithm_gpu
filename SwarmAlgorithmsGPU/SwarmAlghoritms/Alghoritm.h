#pragma once

#include "AlghoritmSettings.h"
#include "TimeMeasurable.h"
#include "Releasable.h"

class Alghoritm : public TimeMeasurable
{
public:
	class Particle;
	struct BestSolution;
protected:
	//osobniki
	Particle * particles;
	//najlepsze globalne rozwi�zanie
	BestSolution * globalBestValue;
	//Macierz wag F
	float *facilities;
	//Macierz odleg�o�ci L
	float *locations;
	//rozmiar problemu
	int problemSize;
	//wsp�czynnik bezw�adno�ci
	float intertia;
	//waga �wiadomo�ci
	float cognitionLR;
	//waga my�lenia spo�ecznego
	float socialLR;
	//R1
	float r1;
	//R2
	float r2;
	//ziarno prawdopodobie�stwa
	int seed;
	//ilo�� cz�stek
	int particleNumber;
	//Losuje warto�ci dla osobnika
	void randomParticle(float * particleSolution,int * rands);
	//bierz�ce iteracje
	int iteration;
	//Limit iteracji - kryteiurm stopu
	int maxIteration;
	//poprzednia najlepsza warto��
	float prevGlobalBestValue;
	//Limit polepszania si� rozwi�zania - kryterium stopu
	float epsilon;
	//Dekodowania rozwi�zania
	virtual void decodeSolution() = 0;
public:
	//Osobnik - struktura
	class Particle : public Releasable
	{
	protected:
		//rozwi�zanie obecne
		float * solution;
		//najlepsze obecne rozwi�zanie
		BestSolution * localBestValue;
		//pr�dko��
		float * velocity;
		//obliczenie pr�dko�ci
		virtual void countVelocity(float intertia, float cognitionLR, float socialLR,
			float r1, float r2, float * globalBest, int problemSize) = 0;
		//Skalowanie rozwi�zania
		virtual void scaleSolution(int problemSize) = 0;
	public:
		//krok - obliczenie obecnej bestValue
		virtual void CheckBestValue(float * facilities, float * locations, int problemSize) = 0;
		//krok - generacja nowego osobnika
		virtual void Move(float intertia, float cognitionLR, float socialLR,
			float r1, float r2, float * globalBest, int problemSize) = 0;
		//konstruktor publiczny
		Particle();
		//Inicjalizacja
		virtual void InitParticle(float * initSolution, float * facilities, float * locations, int problemSize);
		//zwalnie pami�c z osobnika
		virtual void Release(int n);
		//Pobiera najlepsze rozwi�zanie
		BestSolution GetBestSolution() const { return *localBestValue; }
	};
	//klasa najlepszego rozwi�zania
	struct BestSolution : public Releasable
	{
		//najlepsza warto�� funkcji celu
		float Value;
		//najlepsze rozwi�zanie
		float * Solution;
		//Zwalnia pami�� z rozwi�zania
		virtual void Release(int n);
		//por�wnanie rozwi�zania
		bool operator<(BestSolution & other) const { return this->Value < other.Value;  }
		//Konstruktor
		BestSolution();
	};
	//funkcja celu
	Alghoritm(AlghoritmSettings settings);
	//kryterium stopu
	bool IsStop();
	//Aktualizacja najlepszej globalnej warto�ci
	void UpdateGlobalBest();
	//Aktualizacja polo�enia
	virtual void MoveParticles() = 0;
	//aktualizacja lokalnych warto�ci osobnik�w
	virtual void UpdateParticlesValues() = 0;
	//inicjalizacja osobnik�w
	virtual void ParticleInit() = 0;
	//inicjalizacja parametr�w
	void ParametersInit();
	//Rozpocz�cie optymalizacji
	void Start();
	//Zako�czenie algorytmu
	void Finish();
	//destruktor
	~Alghoritm(void);
};

