#include<cstring>
#include<cstdlib>

#include "AlghoritmSettings.h"
#include "Literals.h"
#include "MatrixHelper.h"
#include "DebugHelper.h"

AlghoritmSettings::AlghoritmSettings(int argc, char * argv[])
{
	LogMessage("Loading alghoritm settings");
	for(int i = 0; i < argc; i++)
	{
		if(strcmp(PROBLEM_SIZE_LITERAL,argv[i]) == 0)
		{
			problemSize = atoi(argv[++i]);
			PrintValue(problemSize,"problemSize - settings");
		}
		else if(strcmp(FACILITIES_LITERAL,argv[i]) == 0)
		{
			i = parseDataMatrix(argv,i+1,&facilities);
			PrintMatrix(facilities,problemSize,"locations - settings");
		}
		else if(strcmp(LOCATIONS_LITERAL,argv[i]) == 0)
		{
			i = parseDataMatrix(argv,i+1,&locations);
			PrintMatrix(facilities,problemSize,"facilities - settings");
		}
		else if(strcmp(INTERIA_LITERAL,argv[i]) == 0)
		{
			intertia = (float)atof(argv[++i]);
			PrintValue(intertia,"interia - settings");
		}
		else if(strcmp(COGNITION_LITERAL,argv[i]) == 0)
		{
			cognitionLR = (float)atof(argv[++i]);
			PrintValue(cognitionLR,"cognition lerning factor - settings");
		}
		else if(strcmp(SOCIAL_LITERAL,argv[i]) == 0)
		{
			socialLR = (float)atof(argv[++i]);
			PrintValue(socialLR,"social learning factor - settings");
		}
		else if(strcmp(R1_LITERAL,argv[i]) == 0)
		{
			r1 = (float)atof(argv[++i]);
			PrintValue(r1,"R1 - settings");
		}
		else if(strcmp(R2_LITERAL,argv[i]) == 0)
		{
			r2 = (float)atof(argv[++i]);
			PrintValue(r2,"R2 - settings");
		}
		else if(strcmp(SEED_LITERAL,argv[i]) == 0)
		{
			seed = atoi(argv[++i]);
			PrintValue(seed,"seed - settings");
		}
		else if(strcmp(PARTICLE_NUMBER_LITERAL,argv[i]) == 0)
		{
			particleNumber = atoi(argv[++i]);
			PrintValue(particleNumber,"particle number - settings");
		}
		else if(strcmp(MAX_ITERATION_LITERAL,argv[i]) == 0)
		{
			maxIteration = atoi(argv[++i]);
			PrintValue(maxIteration,"maxIteration - settings");
		}
		else if(strcmp(EPSILON_LITERAL,argv[i]) == 0)
		{
			epsilon = (float)atof(argv[++i]);
			PrintValue(epsilon,"episilon - settings");
		}
		else if(strcmp(ALGHORITM_TYPE_LITERAL,argv[i]) == 0)
		{
			alghoritm_type = atoi(argv[++i]);
			PrintValue(alghoritm_type,"alghoritm_type - settings");
		}
		else if(strcmp(OUTPUT_FILE_LITERAL,argv[i]) == 0)
		{
			outputPath = new char[strlen(argv[++i]) + 1];
			strcpy(outputPath,argv[i]);
			LogMessage(outputPath);
		}
	}
}

AlghoritmSettings::AlghoritmSettings(AlghoritmSettings & copy)
{
	LogMessage("Copy settings");
	problemSize = copy.GetProblemSize();
	PrintValue(problemSize,"problemSize - copied");
	DeepCopyMatrix(copy.GetFacilities(),&facilities,problemSize);
	PrintMatrix(facilities,problemSize,"facilities - copied");
	DeepCopyMatrix(copy.GetLocations(),&locations,copy.problemSize);
	PrintMatrix(locations,problemSize,"locations - copied");
	intertia = copy.GetIntertia();
	PrintValue(intertia,"intertia - copied");
	cognitionLR = copy.GetCognitionLR();
	PrintValue(cognitionLR,"cognitionLR - copied");
	socialLR = copy.GetSocialLR();
	PrintValue(socialLR,"socialLR - copied");
	r1 = copy.GetR1();
	PrintValue(r1,"r1 - copied");
	r2 = copy.GetR2();
	PrintValue(r2,"r2 - copied");
	seed = copy.GetSeed();
	PrintValue(seed,"seed - copied");
	particleNumber = copy.GetParticleNumber();
	PrintValue(particleNumber,"particleNumber - copied");
	epsilon = copy.GetEpisilon();
	PrintValue(epsilon,"epsilon - copied");
	maxIteration = copy.GetmMaxIteration();
	PrintValue(maxIteration,"maxIteration - copied");
}

void AlghoritmSettings::initFields()
{
	LogMessage("Initializing setting object fields");
	problemSize = 0;
	facilities = NULL;
	locations = NULL;
}

int AlghoritmSettings::parseDataMatrix(char * argv[], int startIndex, float ** matrix)
{
	NewMatrix(matrix,problemSize);
	for(int i = 0; i < problemSize; i++)
	{
		for(int j = 0; j < problemSize; j++)
			(*matrix)[i*problemSize + j] = (float)atof(argv[startIndex++]);
	}
	PrintMatrix(*matrix,problemSize,"matrix - parsed");
	return --startIndex;
}

void AlghoritmSettings::Release(int n)
{
	delete[] outputPath;
}

AlghoritmSettings::~AlghoritmSettings(void)
{
	LogMessage("Releasing setting object");
	ReleaseMatrix(facilities,problemSize);
	ReleaseMatrix(locations,problemSize);
}
