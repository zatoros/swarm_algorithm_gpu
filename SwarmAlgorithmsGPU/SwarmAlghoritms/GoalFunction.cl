
__kernel void goalFunction(__global float* result, __global float* facilities,__global float* locations,
							__global float* solution,
							const int problemSize)
{
	int i = get_global_id(0);
	float sum = 0;
	for(int j = 0; j < problemSize; j++)
	{
		for(int r = 0; r < problemSize; r++)
		{
			for(int s = 0; s < problemSize; s++)
			{
				sum += facilities[i*problemSize + j]*locations[r*problemSize + s]*solution[i*problemSize + r]*solution[j*problemSize + s];
			}
		}
	}
	result[i] = sum;
}