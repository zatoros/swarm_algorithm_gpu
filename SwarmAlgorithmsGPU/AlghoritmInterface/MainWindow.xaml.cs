﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Microsoft.Win32;

namespace AlghoritmInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Literals

        const string TEST_DATA_PATH_LITERAL = "TestDataPath";

        const string CONSOLE_APP_PATH_LITERAL = "ConsoleApplicationPath";

        const string TEST_DATA_PATH_DATA_LITERAL = "qapdata";

        const string TEST_DATA_PATH_DATA_EXTENSION_LITERAL = ".dat";

        static readonly string[] ALGHORITM_NAMES_LITERALS = { "Algorytm CPU", "Algorytm AMP", "Algorytm OpenCL" };

        #region Error Literals

        const string NULL_VALUE_LITERAL = "Pole nie może być puste";

        const string R_VALUE_LITERAL = "Wartość musi mieścić się w przedziale (0,1)";

        const string POSITIVE_VALUE_LITERAL = "Wartość musi być większa od 0";

        const string EPSILON_LITERAL = "Wartość musi być większa od 0 (lub równa -1)";

        #endregion Error Literals

        #endregion Literals

        #region Variables

        private string testDataPath;

        private string consoleApplicationPath;

        #endregion Variables

        #region Properties

        public string TestInstance
        {
            get
            {
                if (cbTestInstance.SelectedItem == null)
                    return string.Empty;
                return cbTestInstance.SelectedItem.ToString();
            }
        }

        public int? ProblemSize
        {
            get
            {
                if (!string.IsNullOrEmpty(TestInstance))
                {
                    return Regex.Match(TestInstance, "[0-9]+").Value.ToSafeInt(); 
                }
                else
                {
                    return tbProblemSize.Text.ToSafeInt();
                }
            }
        }

        public float? Interia
        {
            get
            {
                return tbInteria.Text.ToSafeFloat();
            }
        }

        public float? CognitionLR
        {
            get
            {
                return tbCognition.Text.ToSafeFloat();
            }
        }

        public float? SocialLR
        {
            get
            {
                return tbSocial.Text.ToSafeFloat();
            }
        }

        public float? R1
        {
            get
            {
                return tbR1.Text.ToSafeFloat();
            }
        }

        public float? R2
        {
            get
            {
                return tbR2.Text.ToSafeFloat();
            }
        }

        public int? ParticleNumber
        {
            get
            {
                return tbParticleNumber.Text.ToSafeInt();
            }
        }

        public int? MaxIteration
        {
            get
            {
                return tbMaxIteration.Text.ToSafeInt();
            }
        }

        public float? Epsilon
        {
            get
            {
                return tbEpsilon.Text.ToSafeFloat();
            }
        }


        public int? Seed
        {
            get
            {
                return tbSeed.Text.ToSafeInt();
            }
        }

        public int AlghoritmType
        {
            get
            {
                return cbAlghoritmType.SelectedIndex;
            }
        }

        public string ReportPath
        {
            get
            {
                return tbReportPath.Text;
            }
        }

        public List<float> Facilities { set; get; }

        public List<float> Locations { set; get; }

        #endregion Properties

        #region Initialization

        public MainWindow()
        {
            InitializeComponent();

            LogMessage("Inicjalizacja aplikacji");

            getTestDataPath();
            fillTestDataComboBox();
            fillAlghoritmComboBox();
        }

        private void getTestDataPath()
        {
            testDataPath = ConfigurationManager.AppSettings[TEST_DATA_PATH_LITERAL] as string;
            consoleApplicationPath = ConfigurationManager.AppSettings[CONSOLE_APP_PATH_LITERAL] as string;
        }

        private void fillTestDataComboBox()
        {
            DirectoryInfo testDirectoryInfo = new DirectoryInfo(System.IO.Path.Combine(testDataPath, TEST_DATA_PATH_DATA_LITERAL));
            cbTestInstance.Items.Add(string.Empty);
            foreach (FileInfo fileInfo in testDirectoryInfo.GetFileSystemInfos("*" + TEST_DATA_PATH_DATA_EXTENSION_LITERAL))
            {
                cbTestInstance.Items.Add(fileInfo.Name.Replace(fileInfo.Extension, string.Empty));
            }
        }

        private void fillAlghoritmComboBox()
        {
            foreach (string item in ALGHORITM_NAMES_LITERALS)
                cbAlghoritmType.Items.Add(item);
            cbAlghoritmType.SelectedIndex = 0;
        }

        #endregion Initialization

        #region Control Functions

        private void EnableTestParametersControls(bool enable)
        {
            tbProblemSize.IsEnabled = enable;
            tbProblemSize.Text = string.Empty;
            IndicateError(tbProblemSize, false);

            bFacilitiesMatrix.IsEnabled = enable;
            bLocationsMatrix.IsEnabled = enable;
        }

        #endregion Control Functions

        #region Validation

        private bool HasError(Control control)
        {
            System.Windows.Controls.ToolTip toolTip = control.ToolTip as System.Windows.Controls.ToolTip;
            if (toolTip != null)
            {
                return toolTip.Visibility == System.Windows.Visibility.Visible;
            }
            return false;
        }

        private void IndicateError(Control control, bool haveError, string message = "")
        {
            System.Windows.Controls.ToolTip toolTip = control.ToolTip as System.Windows.Controls.ToolTip;

            if (haveError)
            {
                control.Background = new SolidColorBrush(Color.FromArgb(255, 255, 128, 128));
                if (toolTip != null)
                {
                    toolTip.Content = message;
                    toolTip.Visibility = System.Windows.Visibility.Visible;
                }
            }
            else
            {
                control.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                if (toolTip != null)
                {
                    toolTip.Content = string.Empty;
                    toolTip.Visibility = System.Windows.Visibility.Hidden;
                }
            }
        }

        private bool CheckNull(Control control, object value)
        {
            if (HasError(control))
                return false;

            bool nullValue = false;
            if (value == null)
            {
                nullValue = true;
            }
            if (value is float?)
            {
                if (!((float?)value).HasValue)
                {
                    nullValue = true;
                }
            }
            else if (value is int?)
            {
                if (!((int?)value).HasValue)
                {
                    nullValue = true;
                }
            }
            else if (value is int)
            {
                if (((int)value) == -1)
                {
                    nullValue = true;
                }
            }
            else if (value is string)
            {
                if(string.IsNullOrEmpty(value.ToString()))
                {
                    nullValue = true;
                }
            }

            if(nullValue)
            {
                IndicateError(control,true,NULL_VALUE_LITERAL);
            }

            return !nullValue;
        }

        private bool CheckPositive(Control control, object value)
        {
            if (value is float?)
            {
                if (((float?)value).Value < 0)
                {
                    IndicateError(control, true, POSITIVE_VALUE_LITERAL);
                    return false;
                }
            }
            return true;
        }

        private bool ValidateForm()
        {
            LogMessage("Walidacja parametrów");
            bool result = true;

            if (result &= CheckNull(tbInteria, Interia))
            {
                result &= CheckPositive(tbInteria, Interia);
            }
            if (result &= CheckNull(tbCognition, CognitionLR))
            {
                result &= CheckPositive(tbCognition, CognitionLR);
            }
            if (result &= CheckNull(tbSocial, SocialLR))
            {
                result &= CheckPositive(tbSocial, SocialLR);
            }
            if (result &= CheckNull(tbR1, R1))
            {
                if (R1 < 0 || R1 > 1.0)
                {
                    result = false;
                    IndicateError(tbR1, true, R_VALUE_LITERAL);
                }
            }
            if (result &= CheckNull(tbR2, R2))
            {
                if (R1 < 0 || R1 > 1.0)
                {
                    result = false;
                    IndicateError(tbR1, true, R_VALUE_LITERAL);
                }
            }
            if (result &= CheckNull(tbParticleNumber, ParticleNumber))
            {
                result &= CheckPositive(tbParticleNumber, ParticleNumber);
            }
            if (result &= CheckNull(tbMaxIteration, MaxIteration))
            {
                result &= CheckPositive(tbMaxIteration, MaxIteration);
            }
            if (result &= CheckNull(tbEpsilon, Epsilon))
            {
                if (Epsilon < 0 && Epsilon != -1)
                {
                    result = false;
                    IndicateError(tbR1, true, EPSILON_LITERAL);
                }
            }
            result &= CheckNull(tbSeed, Seed);
            result &= CheckNull(tbReportPath, ReportPath);

            result &= CheckNull(cbTestInstance, TestInstance);

            return result;
        }

        #endregion Validation

        private void LogMessage(string message)
        {
            tbLog.Text += message + Environment.NewLine;
        }

        private string ParseParemeters()
        {
            StringBuilder paramString = new StringBuilder();

            paramString.Append(Literals.PROBLEM_SIZE_LITERAL);
            paramString.Append(" ");
            paramString.Append(ProblemSize.Value);
            paramString.Append(" ");

            paramString.Append(Literals.FACILITIES_LITERAL);
            paramString.Append(" ");
            foreach (float value in Facilities)
            {
                paramString.Append(value);
                paramString.Append(" ");
            }


            paramString.Append(Literals.LOCATIONS_LITERAL);
            paramString.Append(" ");
            foreach (float value in Locations)
            {
                paramString.Append(value);
                paramString.Append(" ");
            }


            paramString.Append(Literals.INTERIA_LITERAL);
            paramString.Append(" ");
            paramString.Append(Interia.Value);
            paramString.Append(" ");

            paramString.Append(Literals.COGNITION_LITERAL);
            paramString.Append(" ");
            paramString.Append(CognitionLR.Value);
            paramString.Append(" ");

            paramString.Append(Literals.SOCIAL_LITERAL);
            paramString.Append(" ");
            paramString.Append(SocialLR.Value);
            paramString.Append(" ");

            paramString.Append(Literals.R1_LITERAL);
            paramString.Append(" ");
            paramString.Append(R1.Value);
            paramString.Append(" ");

            paramString.Append(Literals.R2_LITERAL);
            paramString.Append(" ");
            paramString.Append(R2.Value);
            paramString.Append(" ");

            paramString.Append(Literals.SEED_LITERAL);
            paramString.Append(" ");
            paramString.Append(Seed.Value);
            paramString.Append(" ");

            paramString.Append(Literals.PARTICLE_NUMBER_LITERAL);
            paramString.Append(" ");
            paramString.Append(ParticleNumber.Value);
            paramString.Append(" ");

            paramString.Append(Literals.EPSILON_LITERAL);
            paramString.Append(" ");
            paramString.Append(Epsilon.Value);
            paramString.Append(" ");

            paramString.Append(Literals.MAX_ITERATION_LITERAL);
            paramString.Append(" ");
            paramString.Append(MaxIteration.Value);
            paramString.Append(" ");

            paramString.Append(Literals.ALGHORITM_TYPE_LITERAL);
            paramString.Append(" ");
            paramString.Append(AlghoritmType);
            paramString.Append(" ");

            paramString.Append(Literals.OUTPUT_FILE_LITERAL);
            paramString.Append(" ");
            paramString.Append(ReportPath);

            return paramString.ToString().Replace(",", ".");
        }

        private void LoadMatrices()
        {
            if (!string.IsNullOrEmpty(TestInstance))
            {
                string path = System.IO.Path.Combine(testDataPath, TEST_DATA_PATH_DATA_LITERAL, TestInstance + TEST_DATA_PATH_DATA_EXTENSION_LITERAL);
                StringBuilder text = new StringBuilder(System.IO.File.ReadAllText(path));
                text.Replace(Environment.NewLine, " ");
                text.Replace("\n", " ");
                string[] numbers = text.ToString().Split(' ');
                
                Facilities = new List<float>();
                Locations = new List<float>();

                float temp;
                int i = 0;
                int matrixCount = ProblemSize.Value * ProblemSize.Value;
                while(!float.TryParse(numbers[i++],out temp));
                while (i < numbers.Length)
                {
                    if (float.TryParse(numbers[i++], out temp))
                    {
                        if (Facilities.Count < matrixCount)
                        {
                            Facilities.Add(temp);
                        }
                        else
                        {
                            Locations.Add(temp);
                            if (Locations.Count > matrixCount)
                                break;
                        }
                    }
                }
            }
        }

        private void PrintResults()
        {
            try
            {
                StringReader reportText = new StringReader(System.IO.File.ReadAllText(ReportPath));

                tbResultAlghoritmTime.Text = int.Parse(reportText.ReadLine()).ToString();
                tbResultInitializationTime.Text = int.Parse(reportText.ReadLine()).ToString();
                tbResultIterationTime.Text = countAverage(reportText.ReadLine().Split(' ')).ToString();
                tbResultUpdateTime.Text = countAverage(reportText.ReadLine().Split(' ')).ToString();
                tbResultMovingTime.Text = countAverage(reportText.ReadLine().Split(' ')).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd", ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        double countAverage(string[] strings)
        {
            List<int> avg = new List<int>();
            int temp;
            foreach (string text in strings)
            {
                if (int.TryParse(text, out temp))
                    avg.Add(temp);
            }
            return avg.Average();
        }

        #region Event Handlers

        private void SaveParametrs_Click(object sender, RoutedEventArgs e)
        {
            LoadMatrices();
            if (ValidateForm())
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.FileName = "Parameters_" + TestInstance;
                dlg.DefaultExt = ".txt";
                dlg.Filter = "Text documents (.txt)|*.txt";

                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    LogMessage("Zapisywanie parametrów");
                    string file = dlg.FileName;
                    string parameters = ParseParemeters();

                    System.IO.File.WriteAllText(file, parameters);
                }
            }
            else
            {
                MessageBox.Show("Formatka zawiera błędy", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearLog_Click(object sender, RoutedEventArgs e)
        {
            tbLog.Text = string.Empty;
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            LoadMatrices();
            if (ValidateForm())
            {
                LogMessage("Start Testu");
                ProcessStartInfo processInfo = new ProcessStartInfo(consoleApplicationPath, ParseParemeters());

                Process pr = Process.Start(processInfo);
                pr.WaitForExit();

                PrintResults();
            }
            else
            {
                MessageBox.Show("Formatka zawiera błędy", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ReportPath_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Report_" + TestInstance; 
            dlg.DefaultExt = ".txt"; 
            dlg.Filter = "Text documents (.txt)|*.txt"; 

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                tbReportPath.Text = dlg.FileName;
            }
        }

        private void TestInstance_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            EnableTestParametersControls(string.IsNullOrEmpty(TestInstance));
        }

        private void FloatTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb != null)
            {
                if (!Regex.IsMatch(tb.Text, @"^-?\d+(,\d+)?$"))
                {
                    IndicateError(tb, true, "Wartość w polu musi być numeryczna");
                }
                else
                {
                    IndicateError(tb, false);
                }
                e.Handled = true;
            }
        }

        private void IntegerTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb != null)
            {
                if (!Regex.IsMatch(tb.Text, @"^-?\d+$"))
                {
                    IndicateError(tb, true, "Wartość w polu musi być całkowita");
                }
                else
                {
                    IndicateError(tb, false);
                }
                e.Handled = true;
            }
        }

        private void NeutralTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb != null)
            {
                IndicateError(tb, false);
            }
            e.Handled = true;
        }

        #endregion Event Handlers
    }

    static class Extensions
    {
        public static int? ToSafeInt(this string text)
        {
            int temp;
            if (int.TryParse(text, out temp))
                return temp;
            else
                return null;
        }

        public static float? ToSafeFloat(this string text)
        {
            float temp;
            if (float.TryParse(text, out temp))
                return temp;
            else
                return null;
        }
    }
}
