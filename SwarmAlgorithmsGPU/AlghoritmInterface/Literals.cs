﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlghoritmInterface
{
    class Literals
    {
        public static readonly string PROBLEM_SIZE_LITERAL = "-n";

        public static readonly string FACILITIES_LITERAL = "-F";

        public static readonly string LOCATIONS_LITERAL = "-L";

        public static readonly string INTERIA_LITERAL = "-i";

        public static readonly string COGNITION_LITERAL = "-clr";

        public static readonly string SOCIAL_LITERAL = "-slr";

        public static readonly string R1_LITERAL = "-r1";

        public static readonly string R2_LITERAL = "-r2";

        public static readonly string SEED_LITERAL = "-s";

        public static readonly string PARTICLE_NUMBER_LITERAL = "-pn";

        public static readonly string EPSILON_LITERAL = "-eps";

        public static readonly string MAX_ITERATION_LITERAL = "-mit";

        public static readonly string ALGHORITM_TYPE_LITERAL = "-alt";

        public static readonly string OUTPUT_FILE_LITERAL = "-outf";
    }
}
