#include <iostream>

#include "DevicesInfo.h"

int main()
{
	//Print information about OpenCL devices
	PrintDevicesInfo();

    // Press Enter, to quit application.
	std::cout << "Press any key to continue" << std::endl;
    std::cin.get();
 
    return 0;
}